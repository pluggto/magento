PluggTo > Magento
==============

Module that integrate Magento with PluggTo
Esse módulo integra o Magento com o PluggTo
--------------


**Funcionalidades**

- Sincronização de produtos
- Sincronização de pedidos



**Instalação**

- 1) Subir o módulo via FTP na pasta root de instalação da loja com o cache ativado
- 2) Acessar a administração da loja e efetuar a limpeza do cache
- 3) Realizar o Logout da administração da loja *(Caso isso não seja feito, um erro 404 aparecerá ao realizar a configuração)*
- 4) Realizar o LogIn na administração da loja e acessar a página de configuração do módulo em Sistema > Configurações > ThirdLevel > PluggTo
- 5) Inserir as 4 credencias de acesso do PluggTo  *(Essas são adquiradas dentro da sua conta no PluggTo)*
- 6) Clicar em testar aplicação e verificar se o módulo se autenticou corretamente no PluggTo
- 7) Configurar demais campos com as opções desejadas, não esquecer de realizar o atrelamento de campo
- 8) Caso sua loja gere NotaFiscal, será necessário a customização da Classe Nfe (ver instruções adiante)
- 9) Pronto, sua loja já está configurada

**Importante**



O Módulo trabalha 100% via CRON, portanto é FUNDAMENTAL que o CRON esteja corretamente configurado no seu servidor e na loja


- Recomendamos que acesse a documentação Magento relativo a configuração de CRON em: http://www.magentocommerce.com/wiki/1_-_installation_and_configuration/how_to_setup_a_cron_job
caso tenha qualquer problema em testar se o CRON está funcionando corretamente, recomendamos a extensão http://www.magentocommerce.com/magento-connect/aoe-scheduler.html



**Integração de nota fiscal**
- O Magento não possui um padrão para geração e armazenamento de Notas Fiscais, por conta disso será necessário realizar essa customização com o desenvolvedor de sua confiança, ou solitar um orçamento a ThirdLevel para que o realize.


**Instruções**

    - 1) Encontre o arquivo app/code/community/ThirdLevel/Pluggto/Model/Nef.exemple.php
    - 2) Renomeie o arquivo somente para Nef.php
    - 3) O método getNfe é o responsável por realizar o atrelamento de NotaFiscal, para isso, é disponibilizado o ID do pedido do Magento que é necessário a nota e o ID da entrega
    - 4) Você deve criar um código que busca pela nota fiscal para esse pedido e entrega informado e retorne um array com os indices:
            - a) nef_key - chave da nota fiscal
            - b) nef_number - número da nota fiscal
            - c) nef_serie - série da nota fiscal
            - d) nef_date - data da nota fiscal
            - e) nef_link - Link de acesso a nota fiscal


  (Você ainda poderá digitar esses campos diretamente no PluggTo, caso não os possua no Magento)
